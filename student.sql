/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3307
 Source Schema         : student

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 30/12/2023 13:23:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login`  (
  `username` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `mobile` varchar(11) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `access` char(2) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT 'B',
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf16 COLLATE = utf16_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('001', '001', '赵信', '16542357412', '安徽合肥', 'B');
INSERT INTO `login` VALUES ('002', '002', '易', '14565231452', '安徽黄山', 'B');
INSERT INTO `login` VALUES ('003', '003', '蔡徐坤', '13315151313', '安徽阜阳', 'B');
INSERT INTO `login` VALUES ('004', '004', '艾希', '14455556666', '北京朝阳', 'B');
INSERT INTO `login` VALUES ('005', '005', '瑞兹', '13344448888', '湖北石家庄', 'B');
INSERT INTO `login` VALUES ('006', '006', '提莫', '17778889999', '湖北石家庄', 'B');
INSERT INTO `login` VALUES ('admin', 'admin', '管理员', '17754024161', '安徽安庆', 'A');

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `学号` varchar(5) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `姓名` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `数据结构` float(4, 2) NULL DEFAULT NULL,
  `软件工程` float(4, 2) NULL DEFAULT NULL,
  `计算机网络` float(4, 2) NULL DEFAULT NULL,
  `单片机原理` float(4, 2) NULL DEFAULT NULL,
  `Java程序设计` float(4, 2) NULL DEFAULT NULL,
  `Python程序设计` float(4, 2) NULL DEFAULT NULL,
  `总分` float(5, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`学号`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf16 COLLATE = utf16_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES ('001', '赵信', 15.00, 35.00, 72.10, 18.00, 36.40, 28.50, 205.00);
INSERT INTO `score` VALUES ('002', '易', 48.00, 56.50, 78.00, 88.00, 90.00, 95.00, 455.50);
INSERT INTO `score` VALUES ('003', '蔡徐坤', 1.00, 1.00, 0.10, 0.10, 0.10, 0.20, 2.50);
INSERT INTO `score` VALUES ('004', '艾希', 85.00, 89.50, 92.00, 78.00, 96.00, 96.00, 536.50);
INSERT INTO `score` VALUES ('005', '瑞兹', 48.00, 86.00, 78.00, 96.50, 35.00, 48.00, 391.50);

-- ----------------------------
-- Table structure for studentmanagement
-- ----------------------------
DROP TABLE IF EXISTS `studentmanagement`;
CREATE TABLE `studentmanagement`  (
  `学籍号` varchar(10) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `学号` varchar(5) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `姓名` varchar(8) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `性别` char(2) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `出生日期` date NULL DEFAULT NULL,
  `民族` char(2) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `学院` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `专业` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `班级` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `宿舍号` char(5) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `联系方式` char(11) CHARACTER SET utf16 COLLATE utf16_general_ci NULL DEFAULT NULL,
  `家庭地址` varchar(255) CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  PRIMARY KEY (`学籍号`) USING BTREE,
  UNIQUE INDEX `学籍号`(`学籍号`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf16 COLLATE = utf16_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of studentmanagement
-- ----------------------------
INSERT INTO `studentmanagement` VALUES ('G001', '001', '赵信', '男', '2023-07-20', '汉', '信息学院', '计算机科学与技术', '计科01', '3#213', '16542357412', '安徽合肥');
INSERT INTO `studentmanagement` VALUES ('G002', '002', '易', '男', '2023-11-30', '汉', '数学院', '应用数学', '数学01', '2#101', '14565231452', '安徽黄山');
INSERT INTO `studentmanagement` VALUES ('G003', '003', '蔡徐坤', '男', '2021-10-15', '蒙古', '材料学院', '高分子材料与工程', '材料02', '7#512', '13315151313', '安徽阜阳');
INSERT INTO `studentmanagement` VALUES ('G004', '004', '艾希', '女', '2010-10-15', '汉', '艺术学院', '室内设计', '设计02', '4#322', '14455556666', '北京朝阳');
INSERT INTO `studentmanagement` VALUES ('G005', '005', '瑞兹', '男', '1995-07-13', '汉', '魔法学院', '符文打造', '符文02', '7#122', '13344448888', '湖北石家庄');
INSERT INTO `studentmanagement` VALUES ('G006', '006', '提莫', '男', '2003-01-18', '汉', '侦查学院', '侦查技术', '侦查08', '3#102', '17778889999', '湖北石家庄');

SET FOREIGN_KEY_CHECKS = 1;
